class Calculator:
    def __init__(self):
        self._result = 0

    @staticmethod
    def validate_args(*values):
        if len(values) == 0:
            raise TypeError
        for i in values:
            if not isinstance(i, (int, float, complex)):
                raise TypeError

    @staticmethod
    def validate_two_args(x, y):
        if not isinstance(x, (int, float, complex)) and not isinstance(y, (int, float, complex)):
            raise TypeError

    def setResult(self, result):
        self._result = result

    def result(self):
        return self._result

    def add(self, *values):
        self.validate_args(*values)
        total = 0
        for i in values:
            total += i
        self.setResult(total)
        return total

    def product(self, *values):
        self.validate_args(*values)
        total = 1
        for i in values:
            total *= i
        self.setResult(total)
        return total

    def subtract(self, x, y):
        self.validate_two_args(x, y)
        total = x - y
        self.setResult(total)
        return total

    def divide(self, x, y):
        self.validate_two_args(x, y)
        total = x / y
        self.setResult(total)
        return total
