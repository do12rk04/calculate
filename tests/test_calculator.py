import unittest

from calculator import calculator


class CalculatorTests(unittest.TestCase):
    def setUp(self):
        self.calculator = calculator.Calculator()

    def test_add(self):
        self.assertEqual(self.calculator.add(5), 5)
        self.assertEqual(self.calculator.add(11, -1, -6), 4)
        self.assertEqual(self.calculator.add(46, 44, 19, 1, 26), 136)

    def test_add_invalid_values(self):
        with self.assertRaises(TypeError):
            self.calculator.add()
        with self.assertRaises(TypeError):
            self.calculator.add("Nine")

    def test_product(self):
        self.assertEqual(self.calculator.product(5), 5)
        self.assertEqual(self.calculator.product(11, -1, -6), 66)
        self.assertEqual(self.calculator.product(46, 44, 19, 1, 26), 999856)

    def test_product_invalid_values(self):
        with self.assertRaises(TypeError):
            self.calculator.product()
        with self.assertRaises(TypeError):
            self.calculator.product("Nine")

    def test_subtract(self):
        self.assertEqual(self.calculator.subtract(1, 6), -5)
        self.assertEqual(self.calculator.subtract(1, 0), 1)

    def test_subtract_invalid_values(self):
        with self.assertRaises(TypeError):
            self.calculator.subtract()
        with self.assertRaises(TypeError):
            self.calculator.subtract(1, 2, 3)
        with self.assertRaises(TypeError):
            self.calculator.subtract([1, 2])
        with self.assertRaises(TypeError):
            self.calculator.subtract("Nine", "Four")

    def test_divide(self):
        self.assertEqual(self.calculator.divide(62, 4), 15.5)
        self.assertEqual(self.calculator.divide(10, -1), -10)

    def test_divide_invalid_values(self):
        with self.assertRaises(TypeError):
            self.calculator.divide()
        with self.assertRaises(TypeError):
            self.calculator.divide(1, 2, 3)
        with self.assertRaises(TypeError):
            self.calculator.divide([1, 2])
        with self.assertRaises(TypeError):
            self.calculator.divide("Nine", "Four")

    def test_divide_by_zero(self):
        with self.assertRaises(ZeroDivisionError):
            self.calculator.divide(10, 0)


if __name__ == "__main__":
    unittest.main(verbosity=2)
